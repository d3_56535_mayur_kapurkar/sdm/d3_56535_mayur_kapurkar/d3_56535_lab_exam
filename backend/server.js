const express = require('express')
const routerMovies = require('./routes/movies')

const app = express();

//provide the parse
app.use(express.json())


// use routerMovie
app.use('/movie',routerMovies);

app.listen(4000,()=>{
    console.log("Server started on Port 4000");
});
